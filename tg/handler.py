import os
import json
import random

import telebot.types

import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo

import poll_data.global_vars as pdg
from poll_data.game_poll import GamePoll

# Action handlers
from action_handlers.add_game import handle_add_game_action
from action_handlers.add_player import handle_add_player_action
from action_handlers.change_player_name import handle_change_player_name_action
from action_handlers.delete_game import handle_delete_game_action
from action_handlers.edit_game import handle_edit_game_action
from action_handlers.kick_player import handle_kick_player_action
from action_handlers.message_players import handle_message_players_action

from menus.edit_game.tg.handler import handle_tg_edit_game_menu
from menus.game_info.tg.handler import handle_tg_game_info_menu
from menus.kick_player.tg.handler import handle_tg_kick_player_menu
from menus.list_games.tg.handler import handle_tg_list_games_menu

# Main menu
from menus.main.header import get_main_menu_header
from menus.main.tg.content import create_tg_main_menu
from menus.main.tg.handler import handle_tg_main_menu

from tg.vars import tg_admins
from tg.vars import tg_bot
from tg.vars import tg_lock

from utils.chat_id_utils import get_tg_chat_ids, save_tg_chat_id, del_tg_chat_id
from utils.session_utils import delete_session, get_session, save_session
from utils.file_utils import save_exception
from utils.file_utils import save_to_file

from utils.dice_roller import create_message
from utils.dice_roller import roll_fate
from utils.dice_roller import roll_standard
from utils.dice_roller import roll_v5


def check_membership(user_id, chat_id):
    ok = False
    for saved_chat_id in get_tg_chat_ids():
        try:
            with tg_lock:
                tg_bot.get_chat_member(saved_chat_id, user_id)
            ok = True
            break
        except:
            pass
    if ok:
        return True
    with tg_lock:
        tg_bot.send_message(chat_id, 'Прости, но ты не состоишь ни в одном из чатов, в которых я работаю :(')
    return False

def create_tagged_message(user):
    if user.username is not None:
        return create_message(f'{user.full_name} (@{user.username})')
    else:
        return create_message(f'<a href="tg://user?id={user.id}">{user.full_name}</a>')


def remove_command(text: str, command: str):
    with tg_lock:
        bot_tag = f'@{tg_bot.user.username}'

    command_less = text.replace(f'/{command} ', '')
    command_less = command_less.replace(f'/{command}{bot_tag} ', '')
    return command_less


@tg_bot.message_handler(commands=['add_auf'])
def handle_add_auf(message):
    if message.from_user.id not in tg_admins:
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Упс! Кажется, у Вас недостаточно прав!')
        return

    new_quote = remove_command(message.text, 'add_auf')

    with open('./data/wolf_quotes.json', encoding='utf-8') as f:
        quotes_file = json.load(f)

    quotes_file["quotes"].append(new_quote)
    save_to_file('./data/wolf_quotes.json', json.dumps(quotes_file, indent=4, ensure_ascii=False))

    with tg_lock:
        tg_bot.send_message(message.chat.id, f'Отлично, добавил "{new_quote}"!')


@tg_bot.message_handler(commands=['kick'])
def handle_kick(message):
    if message.from_user.id in tg_admins:
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Хорошо, пока! 👋', parse_mode='HTML')
            tg_bot.leave_chat(message.chat.id)
    else:
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Упс, похоже у Вас нет на это прав!', parse_mode='HTML')

@tg_bot.message_handler(commands=['auf'])
def handle_auf(message):
    f = open('./data/wolf_quotes.json')
    quotes = json.load(f)["quotes"]
    f.close()

    msg = f'"{random.choice(quotes)}" - АУФ.'
    with tg_lock:
        tg_bot.send_message(message.chat.id, msg)


@tg_bot.message_handler(commands=['rf', 'roll_fate'])
def handle_roll_fate(message):
    if not check_membership(message.from_user.id, message.chat.id):
        return
    with tg_lock:
        tg_bot.send_message(message.chat.id, create_tagged_message(message.from_user) + roll_fate(), parse_mode='HTML')


@tg_bot.message_handler(commands=['rv5', 'roll_v5'])
def handle_roll_v5(message):
    if not check_membership(message.from_user.id, message.chat.id):
        return

    arg_str = remove_command(message.text, 'rv5')
    arg_str = remove_command(arg_str, 'roll_v5')

    try:
        pool = int(arg_str.split(' ')[0])
        hunger = int(arg_str.split(' ')[1])
        with tg_lock:
            tg_bot.send_message(message.chat.id, create_tagged_message(message.from_user) + roll_v5(pool, hunger), parse_mode='HTML')
    except:
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Ошибка в команде', parse_mode='HTML')


@tg_bot.message_handler(commands=['r', 'roll'])
def handle_roll_standard(message):
    if not check_membership(message.from_user.id, message.chat.id):
        return

    dice_string = remove_command(message.text, 'r')
    dice_string = remove_command(dice_string, 'roll')

    with tg_lock:
        tg_bot.send_message(message.chat.id, create_tagged_message(message.from_user) + roll_standard(dice_string), parse_mode='HTML')


@tg_bot.message_handler(commands=['advertise'])
def handle_advertise(message):
    if not check_membership(message.from_user.id, message.chat.id):
        return
    with tg_lock:
        tg_bot.send_message(message.chat.id, gdg.game_manager.create_advertisement(f'@{tg_bot.user.username}'), parse_mode='HTML')


@tg_bot.message_handler(commands=['register_chat'])
def handle_register_chat(message):
    if message.from_user.id in tg_admins:
        save_tg_chat_id(message.chat.id)
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Ок, теперь я буду работать с людьми из этого чата', parse_mode='HTML')
    else:
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Упс, похоже у Вас нет на это прав!', parse_mode='HTML')


@tg_bot.message_handler(commands=['unregister_chat'])
def handle_unregister_chat(message):
    if message.from_user.id in tg_admins:
        del_tg_chat_id(message.chat.id)
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Ок, теперь я не буду работать с людьми из этого чата', parse_mode='HTML')
    else:
        with tg_lock:
            tg_bot.send_message(message.chat.id, 'Упс, похоже у Вас нет на это прав!', parse_mode='HTML')


@tg_bot.message_handler(commands=['del'])
def handle_delete(message):
    if message.reply_to_message is not None:
        if message.reply_to_message.from_user.id == tg_bot.user.id:
            tg_bot.delete_message(message.reply_to_message.chat.id, message.reply_to_message.message_id)


@tg_bot.message_handler(commands=['start'])
def handle_start(message):
    if not check_membership(message.from_user.id, message.chat.id):
        return

    if message.chat.id != message.from_user.id:
        with tg_lock:
            tg_bot.send_message(message.chat.id, "Привет! Я работаю только в ЛС!", parse_mode='HTML')
        return

    with tg_lock:
        tg_bot.send_message(message.chat.id, get_main_menu_header(), reply_markup=create_tg_main_menu(), parse_mode='HTML')


@tg_bot.message_handler(commands=['cancel'])
def handle_cancel(message):
    if not check_membership(message.from_user.id, message.chat.id):
        return

    if message.chat.id != message.from_user.id:
        return

    with tg_lock:
        tg_bot.send_message(message.chat.id, delete_session(f'./data/sessions/tg/{message.from_user.id}'), parse_mode='HTML')


@tg_bot.message_handler(content_types=['poll'])
def handle_poll(message: telebot.types.Message):
    if not check_membership(message.from_user.id, message.chat.id):
        return

    if message.chat.id != message.from_user.id:
        return

    path = f'./data/sessions/tg/{message.from_user.id}'
    if os.path.exists(path):
        player = PlayerInfo(message.from_user.full_name, 'Telegram', message.from_user.id)
        session = get_session(path)
        if session["type"] != 'message-players':
            return
        if session["stage"] != 'compose':
            return

        tg_poll: telebot.types.Poll = message.poll
        answers = list()
        for answer in tg_poll.options:
            answers.append(answer.text)

        poll = GamePoll(0, session["game_id"], player, tg_poll.question, answers, [], [], tg_poll.allows_multiple_answers)

        session["stage"] = 'poll'
        session["poll"] = poll.to_json()
        save_session(path, session)
        repl = handle_message_players_action('', path, player)
        if repl:
            with tg_lock:
                tg_bot.send_message(message.chat.id, repl, parse_mode='HTML')


@tg_bot.poll_answer_handler(func=lambda call: True)
def handle_poll_answer(poll_answer: telebot.types.PollAnswer):
    player = PlayerInfo(poll_answer.user.full_name, 'Telegram', poll_answer.user.id)
    if not poll_answer.option_ids:
        tg_bot.send_message(poll_answer.user.id, 'Извините, но отмена голоса учтена не будет. Если Вы ошиблись - напишите ведущему', parse_mode='HTML')
        return
    pdg.poll_manager.vote_poll(poll_answer.poll_id, poll_answer.option_ids, player)


@tg_bot.message_handler(content_types=['text'])
def handle_text(message):
    if not check_membership(message.from_user.id, message.chat.id):
        return

    if message.text.startswith('/'):
        return

    path = f'./data/sessions/tg/{message.from_user.id}'
    if os.path.exists(path):
        player = PlayerInfo(message.from_user.full_name, 'Telegram', message.from_user.id)
        session = get_session(path)

        repl = None
        if session["type"] == 'add-game':
            repl = handle_add_game_action(message.text, path, player)
        elif session["type"] == "message-players":
            repl = handle_message_players_action(message.text, path, player)
        elif session["type"] == "delete-game":
            repl = handle_delete_game_action(message.text, path, player)
        elif session["type"] == "edit-game":
            repl = handle_edit_game_action(message.text, path, player)
        elif session["type"] == "change-player-name":
            repl = handle_change_player_name_action(message.text, path, player)
        elif session["type"] == "kick-player":
            repl = handle_kick_player_action(message.text, path, player)
        elif session["type"] == "add-player":
            repl = handle_add_player_action(message.text, path, player)

        if repl:
            with tg_lock:
                tg_bot.send_message(message.chat.id, repl, parse_mode='HTML')


@tg_bot.callback_query_handler(func=lambda call: True)
def main_callback(call):
    chat_id = call.message.chat.id
    message_id = call.message.message_id
    user = call.from_user

    path = f'./data/sessions/tg/{user.id}'
    if os.path.exists(path):
        delete_session(path)

    if not check_membership(user.id, chat_id):
        return

    if chat_id != user.id:
        with tg_lock:
            tg_bot.send_message(chat_id, "Привет! Я работаю только в ЛС!", parse_mode='HTML')
        return

    reply = None
    if call.data.startswith('main_'):
        command = call.data.replace('main_', '')
        reply = handle_tg_main_menu(command, chat_id, message_id, user)
    elif call.data.startswith('list-games_'):
        command = call.data.replace('list-games_', '')
        reply = handle_tg_list_games_menu(command, chat_id, message_id, user)
    elif call.data.startswith('game-info_'):
        command = call.data.replace('game-info_', '')
        reply = handle_tg_game_info_menu(command, chat_id, message_id, user)
    elif call.data.startswith('edit-game_'):
        command = call.data.replace('edit-game_', '')
        reply = handle_tg_edit_game_menu(command, chat_id, message_id, user)
    elif call.data.startswith('kick-player_'):
        command = call.data.replace('kick-player_', '')
        reply = handle_tg_kick_player_menu(command, chat_id, message_id, user)
    else:
        reply = "Внутренняя ошибка!"
    if reply:
        with tg_lock:
            tg_bot.answer_callback_query(callback_query_id=call.id, text=reply, show_alert=True)
    else:
        with tg_lock:
            tg_bot.answer_callback_query(callback_query_id=call.id)


def run_tg_bot():
    while True:
        try:
            tg_bot.infinity_polling()
        except Exception as e:
            save_exception('run_tg_bot', e)
            continue
