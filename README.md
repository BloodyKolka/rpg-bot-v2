# RPG Бот v2
Бот-менеджер встреч по НРИ (Настольным Ролевыи Играм)

## Функционал
### Ведущий
- Cоздать игру
- Редактировать игру
- Добавить игрока +1
- Выгнать игрока
- Открыть запись на игру
- Закрыть запись на игру
- Отправить сообщение игрокам
- Удалить игру
### Игрок
- Записаться на игру
- Отменить запись на игру
### Поддерживаемые платформы
- Telegram
- ВКонтакте

## Установка
1. Установка зависимостей: бот зависит от `pyTelegramBotApi` и `vk-api` (устанавливаются через `pip`)
2. Создание конфигурационных файлов
   1. `tg/vars.py`:
   2. `vk/vars.py`:
3. Запуск бота `python3 bot.py`

## Конфигурационные файлы
`tg/vars.py`:
```Python
import telebot
import threading

tg_admins = [<telegram admin user ID>]
tg_bot_token = '<Telegram Token>'
tg_bot = telebot.TeleBot(tg_bot_token)
tg_lock = threading.Lock()

```
`vk/vars.py`:
```Python
import vk_api
import threading

vk_admins = ['<VK admin user id>']
vk_group_id = <group id>
vk_group_chats = [<group chat ids>]
vk_token = '<VK Token>'
vk_session = vk_api.VkApi(token=vk_token)
vk = vk_session.get_api()
vk_lock = threading.Lock()

```
`<group chat ids>` - ID чатов сообщества

## Скриншоты
### Telegram
Главное меню:

![Telegram Main Menu](./pictures/tg/main_menu.png)

Меню списка игр:

![Telegram List Games Menu](./pictures/tg/list_games_menu.png)

Меню информации об игре (взгляд ведущего):

![Telegram Game Info Menu (GM)](./pictures/tg/game_info_gm.png)

Меню информации об игре (взгляд незарегистрированного игрока):

![Telegram Game Info Menu (Unregistered Player)](./pictures/tg/game_info_player_unregistered.png)

Меню информации об игре (взгляд зарегистрированного игрока):

![Telegram  Game Info Menu (Registered Player)](./pictures/tg/game_info_player_registered.png)

Меню редактирования информации об игре:

![Telegram Edit Game Menu](./pictures/tg/edit_game_menu.png)

Меню изгнания игрока:

![Telegram Kick Player Menu Menu](./pictures/tg/kick_player_menu.png)

### ВКонтакте
Главное меню:

![VK Main Menu](./pictures/vk/main_menu.png)

Меню списка игр:

![VK List Games Menu](./pictures/vk/list_games_menu.png)

Меню информации об игре (взгляд ведущего):

![VK Game Info Menu (GM)](./pictures/vk/game_info_gm.png)

Меню информации об игре (взгляд незарегистрированного игрока):

![VK Game Info Menu (Unregistered Player)](./pictures/vk/game_info_player_unregistered.png)

Меню информации об игре (взгляд зарегистрированного игрока):

![VK Game Info Menu (Registered Player)](./pictures/vk/game_info_player_registered.png)

Меню редактирования информации об игре (1-я страница):

![VK Edit Game Menu Page 1](./pictures/vk/edit_game_menu_p1.png)

Меню изгнания игрока:

![VK Kick Player Menu Menu](./pictures/vk/kick_player_menu.png)
