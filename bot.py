import threading

from tg.handler import run_tg_bot
from vk.handler import run_vk_bot

tg_thread = threading.Thread(target=run_tg_bot)
vk_thread = threading.Thread(target=run_vk_bot)

tg_thread.start()
vk_thread.start()

tg_thread.join()
vk_thread.join()
