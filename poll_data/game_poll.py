from dataclasses import dataclass
import json

from game_data.player_info import PlayerInfo
from utils.get_player_tag import get_player_tag


@dataclass
class GamePoll:
    def vote(self, answers: list[int], player: PlayerInfo):
        self.votes.append(answers)
        self.voted_players.append(player)

    def get_stats_message(self, platform: str):
        total_votes = len(self.voted_players)

        results = [0] * len(self.options)
        vote_data = [[]] * len(self.options)
        for i in range(0, len(vote_data)):
            vote_data[i] = list()

        for i, vote in enumerate(self.votes):
            for option in vote:
                results[option] += 1
                vote_data[option].append(self.voted_players[i])

        stats = str()
        stats += f'{self.question}\n'
        for i, option in enumerate(self.options):
            stats += f'[{i : < 2}] {option} ({round(results[i]/total_votes * 100, 1)}%)\n'
        stats += '\nРаспределение по игрокам:\n'

        for i, option in enumerate(self.options):
            stats += f'[{i : > 2}]: '
            if not vote_data[i]:
                stats += 'Никто не проголосовал за этот вариант\n'
            else:
                for player in vote_data[i]:
                    if player.id != -1:
                        stats += f'{player.name} ({get_player_tag(player, platform)}) '
                    else:
                        try:
                            adding_player = PlayerInfo.from_json(json.loads(player.platform))
                            stats += f'{player.name} (+1 от {adding_player.name} ({get_player_tag(adding_player, platform)})) '
                        except:
                            stats += f'{player.name} (+1) '
                stats += '\n'

        return stats

    def to_json(self):
        voted_players = list()
        for player in self.voted_players:
            voted_players.append(player.to_json())

        res = dict()
        res["poll_id"] = self.poll_id
        res["game_id"] = self.game_id
        res["gm_info"] = self.gm_info.to_json()
        res["question"] = self.question
        res["options"] = self.options
        res["votes"] = self.votes
        res["voted_players"] = voted_players
        res["is_multiple_vote"] = self.is_multiple_vote
        return res

    def save_to_json_file(self, filename: str):
        f = open(filename, 'w')
        json.dump(self.to_json(), f, indent=4)
        f.close()

    @classmethod
    def from_json(cls, obj: dict):
        return cls(obj["poll_id"],
                   obj["game_id"],
                   PlayerInfo.from_json(obj["gm_info"]),
                   obj["question"],
                   obj["options"],
                   obj["votes"],
                   PlayerInfo.from_json_list(obj["voted_players"]),
                   obj["is_multiple_vote"])

    @classmethod
    def from_json_file(cls, filename):
        f = open(filename)
        obj = json.load(f)
        f.close()
        return cls.from_json(obj)

    poll_id: int
    game_id: int
    gm_info: PlayerInfo
    question: str
    options: list[str]
    votes: list[list[int]]
    voted_players: list[PlayerInfo]
    is_multiple_vote: bool

