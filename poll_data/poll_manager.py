import os.path
import shutil
from threading import Lock

import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo

from poll_data.game_poll import GamePoll

from utils.file_utils import save_to_file
from utils.file_utils import get_from_file
from utils.get_player_tag import get_player_tag

from utils.send_message import send_message


class PollManager:
    def delete_all_game_polls(self, game_id: int):
        path = f'{self._work_dir}/{game_id}'
        with self._lock:
            if os.path.exists(path):
                shutil.rmtree(path)

    def get_poll(self, platform_poll_id: str, platform: str):
        with self._lock:
            return self.__load_poll(platform_poll_id, platform)

    def add_poll(self, poll: GamePoll) -> GamePoll:
        with self._lock:
            return self.__save_poll(poll)

    def vote_poll(self, platform_poll_id: str, answers: list[int], player: PlayerInfo):
        with self._lock:
            poll = self.__load_poll(platform_poll_id, player.platform)
            if poll is None:
                return
            poll.vote(answers, player)

            res = gdg.game_manager.get_game(poll.game_id)
            if not res.ok:
                return
            game = res.res

            gm_msg = f"Привет, обновление по Вашей игре\n{game.get_header()}\n"
            gm_msg += f'Игрок {player.name} ({get_player_tag(player, game.gm_info.platform)}) проголосовал в опросе!\n'
            send_message(gm_msg, game.gm_info)
            send_message(poll.get_stats_message(game.gm_info.platform), game.gm_info)

            if len(game.players) == len(poll.voted_players):
                self.__delete_poll(poll)
                return None
            else:
                return self.__save_poll(poll)

    def __delete_poll(self, poll: GamePoll):
        if poll.poll_id == 0:
            return None
        path = f'{self._work_dir}/{poll.game_id}/{poll.poll_id}.json'
        if os.path.exists(path):
            os.remove(path)
        return None

    def __save_poll(self, poll: GamePoll) -> GamePoll:
        if poll.poll_id == 0:
            poll.poll_id = self.__get_latest_poll_id(poll.game_id)
        path = f'{self._work_dir}/{poll.game_id}/{poll.poll_id}.json'
        poll.save_to_json_file(path)
        return poll

    def __load_poll(self, platform_poll_id: str, platform: str):
        if platform == 'Telegram':
            path = f'{self._work_dir}/tg/{platform_poll_id}.txt'
        else:
            return None

        if not os.path.exists(path):
            return None

        id_str = get_from_file(path)
        os.remove(path)

        game_id = int(id_str.split('_')[0])
        poll_id = int(id_str.split('_')[1])

        poll_path = f'{self._work_dir}/{game_id}/{poll_id}.json'

        poll = GamePoll.from_json_file(poll_path)
        return poll

    def __get_latest_poll_id(self, game_id: int):
        path = f'{self._work_dir}/{game_id}'

        if not os.path.exists(path):
            os.makedirs(path)
            save_to_file(f'{path}/latest_poll_id', 1)
            return 1
        else:
            current_id = int(get_from_file(f'{path}/latest_poll_id')) + 1
            save_to_file(f'{path}/latest_poll_id', current_id)
            return current_id

    def __init__(self, work_dir):
        self._work_dir = work_dir
        self._lock = Lock()
