import os
from threading import Lock

from game_data.game import Game
from game_data.result import Result

import poll_data.global_vars as pdg


class GameManager:
    def list_games(self) -> list[Game]:
        res = list()
        with self._lock:
            for file in os.listdir(self._work_dir):
                if file.endswith('.json'):
                    res.append(Game.load_from_json_file(f'{self._work_dir}/{file}'))
        return res

    def create_advertisement(self, bot_info):
        games = self.list_games()
        msg = f'Сейчас для записи доступны следующие игры:\n'

        for game in games:
            if game.registration_open and (game.free_slots > 0):
                msg += game.get_header() + '\n'

        msg += f'Запись в ЛС с помощью бота ({bot_info})'
        return msg

    def add_game(self, game: Game):
        with self._lock:
            f = open(f'{self._work_dir}/latest_game.txt')
            latest_game = int(f.read()) + 1
            f.close()
            game.game_id = latest_game
            game.save_to_json_file(f'{self._work_dir}/{latest_game}.json')
            f = open(f'{self._work_dir}/latest_game.txt', mode='w')
            f.write(f'{latest_game}')
            f.close()

    def delete_game(self, game_id: int):
        path = self._work_dir + f'/{game_id}.json'
        with self._lock:
            if not os.path.exists(path):
                return Result(False, "Упс! Похоже, такой игры не существует или она была удалена!", None)
            os.remove(path)
            pdg.poll_manager.delete_all_game_polls(game_id)
            return Result(True, "Игра успешно удалена!", None)

    def update_game(self, game: Game) -> Result:
        path = self._work_dir + f'/{game.game_id}.json'
        with self._lock:
            if not os.path.exists(path):
                return Result(False, "Упс! Похоже, такой игры не существует или она была удалена!", None)
            game.save_to_json_file(path)
        return Result(True, "Изменения успешно сохранены", None)

    def get_game(self, game_id) -> Result:
        path = self._work_dir + f'/{game_id}.json'
        with self._lock:
            if not os.path.exists(path):
                return Result(False, "Упс! Похоже, такой игры не существует или она была удалена!", None)
            game = Game.load_from_json_file(path)
        return Result(True, "", game)

    def __init__(self, work_dir):
        self._work_dir = work_dir
        self._lock = Lock()
