from dataclasses import dataclass


@dataclass
class CampaignInfo:
    is_oneshot: bool
    name: str
    desc: str
    session: int

    @classmethod
    def from_json(cls, obj: dict):
        return cls(obj["is_oneshot"], obj["name"], obj["desc"], obj["session"])

    def to_json(self):
        res = dict()
        res["is_oneshot"] = self.is_oneshot
        res["name"] = self.name
        res["desc"] = self.desc
        res["session"] = self.session
        return res
