from dataclasses import dataclass


@dataclass
class SessionInfo:
    name: str
    desc: str

    @classmethod
    def from_json(cls, obj: dict):
        return cls(obj["name"], obj["desc"])

    def to_json(self):
        res = dict()
        res["name"] = self.name
        res["desc"] = self.desc
        return res
