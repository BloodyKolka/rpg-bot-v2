from dataclasses import dataclass


@dataclass
class SystemInfo:
    short_name: str
    full_name: str

    @classmethod
    def from_json(cls, obj: dict):
        return cls(obj["short_name"], obj["full_name"])

    def to_json(self):
        res = dict()
        res["short_name"] = self.short_name
        res["full_name"] = self.full_name
        return res
