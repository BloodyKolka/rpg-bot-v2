from dataclasses import dataclass


@dataclass
class PlayerInfo:
    name: str
    platform: str
    id: int

    @classmethod
    def from_json(cls, obj: dict):
        return cls(obj["name"], obj["platform"], obj["id"])

    @classmethod
    def from_json_list(cls, obj: list[dict]):
        res = []
        for item in obj:
            res.append(cls.from_json(item))
        return res

    def to_json(self):
        res = dict()
        res["name"] = self.name
        res["platform"] = self.platform
        res["id"] = self.id
        return res


def is_same_user(a: PlayerInfo, b: PlayerInfo) -> bool:
    return (a.id == b.id) and (a.platform == b.platform)


def is_user_in_list(a: PlayerInfo, player_list: list[PlayerInfo]) -> bool:
    for player in player_list:
        if is_same_user(a, player):
            return True
    return False
