from dataclasses import dataclass


@dataclass
class Result:
    ok: bool
    desc: str
    res: any
