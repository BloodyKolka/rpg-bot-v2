import json
from dataclasses import dataclass

from game_data.campaign_info import CampaignInfo
from game_data.player_info import PlayerInfo
from game_data.result import Result
from game_data.session_info import SessionInfo
from game_data.system_info import SystemInfo


@dataclass
class Game:
    def update_player_name(self, player: PlayerInfo) -> Result:
        reg_num = self.__get_registered_num(player)
        if reg_num != -1:
            self.players[reg_num] = player
            return Result(True, "Имя успешно обновлено!", None)
        return Result(False, "Упс! Кажется, Вы не были зарегистрированы!", None)

    def kick_player(self, num: int):
        if num < len(self.players):
            del self.players[num]
            self.free_slots += 1
            return Result(True, "Игрок успешно удалён!", None)
        return Result(False, "Упс! Кажется, такого игрока нет!", None)

    def add_player(self, player_name: str, adding_player: PlayerInfo):
        if self.free_slots >= 1:
            self.players.append(PlayerInfo(player_name, json.dumps(adding_player.to_json()), -1))
            self.free_slots -= 1
            return Result(True, "Игрок успешно добавлен!", None)
        else:
            return Result(False, "Упс! Кажется, мест больше нет!", None)

    def register(self, player: PlayerInfo) -> Result:
        if not self.registration_open:
            return Result(False, "Упс! Кажется, регистрация на эту игру была закрыта!", None)
        if self.free_slots >= 1:
            self.players.append(player)
            self.free_slots -= 1
            return Result(True, "Вы успешно зарегистрированы!", None)
        else:
            return Result(False, "Упс! Кажется, мест больше нет!", None)

    def unregister(self, player: PlayerInfo) -> Result:
        reg_num = self.__get_registered_num(player)
        if reg_num != -1:
            del self.players[reg_num]
            self.free_slots += 1
            return Result(True, "Регистрация успешно отменена!", None)
        return Result(False, "Упс! Кажется, Вы не были зарегистрированы!", None)

    def get_header(self) -> str:
        if self.registration_open:
            status = 'О'
        else:
            status = 'З'

        if self.campaign_info.is_oneshot:
            name = f'Ваншот "{self.session_info.name}"'
        else:
            name = f'{self.campaign_info.name} (сессия {self.campaign_info.session})'

        return f'[{self.system_info.short_name}][{status}] {name} ({self.free_slots}/{self.total_slots})'

    def __get_registered_num(self, player: PlayerInfo) -> int:
        for i in range(len(self.players)):
            if player.platform == self.players[i].platform:
                if player.id == self.players[i].id:
                    if (player.id != -1) or (player.name == self.players[i].name):
                        return i
        return -1

    @classmethod
    def from_json(cls, obj: dict):
        return cls(obj["game_id"],
                   SystemInfo.from_json(obj["system_info"]),
                   PlayerInfo.from_json(obj["gm_info"]),
                   CampaignInfo.from_json(obj["campaign_info"]),
                   SessionInfo.from_json(obj["session_info"]),
                   obj["date_time"],
                   obj["place"],
                   obj["total_slots"],
                   obj["free_slots"],
                   obj["registration_open"],
                   PlayerInfo.from_json_list(obj["players"]))

    @classmethod
    def load_from_json_file(cls, filename: str):
        f = open(filename)
        game_info = json.load(f)
        f.close()
        return cls(game_info["game_id"],
                   SystemInfo.from_json(game_info["system_info"]),
                   PlayerInfo.from_json(game_info["gm_info"]),
                   CampaignInfo.from_json(game_info["campaign_info"]),
                   SessionInfo.from_json(game_info["session_info"]),
                   game_info["date_time"],
                   game_info["place"],
                   game_info["total_slots"],
                   game_info["free_slots"],
                   game_info["registration_open"],
                   PlayerInfo.from_json_list(game_info["players"]))

    def save_to_json_file(self, filename: str):
        players = list()
        for player in self.players:
            players.append(player.to_json())

        res = dict()
        res["game_id"] = self.game_id
        res["system_info"] = self.system_info.to_json()
        res["gm_info"] = self.gm_info.to_json()
        res["campaign_info"] = self.campaign_info.to_json()
        res["session_info"] = self.session_info.to_json()
        res["date_time"] = self.date_time
        res["place"] = self.place
        res["total_slots"] = self.total_slots
        res["free_slots"] = self.free_slots
        res["registration_open"] = self.registration_open
        res["players"] = players

        f = open(filename, mode='w')
        json.dump(res, f, indent=4)
        f.close()

    game_id: int
    system_info: SystemInfo
    gm_info: PlayerInfo
    campaign_info: CampaignInfo
    session_info: SessionInfo
    date_time: str
    place: str
    total_slots: int
    free_slots: int
    registration_open: bool
    players: list[PlayerInfo]
