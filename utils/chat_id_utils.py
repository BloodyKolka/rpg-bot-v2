import json

def get_tg_chat_ids():
    with open('./data/chat_ids.json', encoding='utf-8') as f:
        obj = json.load(f)
        return obj["tg"]

def save_tg_chat_id(new_id: int):
    with open('./data/chat_ids.json', encoding='utf-8') as f:
        all_ids = json.load(f)
        ids = all_ids["tg"]
    if new_id not in ids:
        ids.append(new_id)
        with open('./data/chat_ids.json', encoding='utf-8', mode='w') as f:
            all_ids["tg"] = ids
            json.dump(all_ids, f, indent=4, ensure_ascii=False)

def del_tg_chat_id(removed_id: int):
    with open('./data/chat_ids.json', encoding='utf-8') as f:
        all_ids = json.load(f)
        ids = all_ids["tg"]
    if removed_id in ids:
        ids.remove(removed_id)
        with open('./data/chat_ids.json', encoding='utf-8', mode='w') as f:
            all_ids["tg"] = ids
            json.dump(all_ids, f, indent=4, ensure_ascii=False)
