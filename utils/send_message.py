from vk_api.utils import get_random_id

from game_data.player_info import PlayerInfo

from utils.html_special import restore_html_special

from tg.vars import tg_bot
from tg.vars import tg_lock
from vk.vars import vk
from vk.vars import vk_lock


def send_message(msg: str, player: PlayerInfo):
    try:
        if player.platform == 'Telegram':
            with tg_lock:
                tg_bot.send_message(player.id, msg, parse_mode='HTML')
        elif player.platform == 'VK':
            with vk_lock:
                vk.messages.send(user_id=player.id, message=restore_html_special(msg), random_id=get_random_id())
    except:
        return
