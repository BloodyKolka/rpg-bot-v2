from vk_api.utils import get_random_id

from game_data.player_info import PlayerInfo

from poll_data.game_poll import GamePoll

from tg.vars import tg_bot
from tg.vars import tg_lock
from vk.vars import vk
from vk.vars import vk_lock

from utils.file_utils import save_to_file


def send_poll(poll: GamePoll, player: PlayerInfo):
    try:
        if player.platform == 'Telegram':
            with tg_lock:
                tg_poll_msg = tg_bot.send_poll(player.id, poll.question, poll.options, is_anonymous=False, allows_multiple_answers=poll.is_multiple_vote)
                tg_poll_id = tg_poll_msg.poll.id
                save_to_file(f'./data/polls/tg/{tg_poll_id}.txt', f'{poll.game_id}_{poll.poll_id}')
        elif player.platform == 'VK':
            with vk_lock:
                vk.messages.send(user_id=player.id, message='[Опросы в ВК пока не поддерживаются]', random_id=get_random_id())
    except:
        return
