from game_data.player_info import PlayerInfo

from tg.vars import tg_bot
from tg.vars import tg_lock

from utils.chat_id_utils import get_tg_chat_ids


def get_tg_user(user_id):
    for chat_id in get_tg_chat_ids():
        try:
            with tg_lock:
                user = tg_bot.get_chat_member(chat_id, user_id).user
            return user
        except:
            pass
    return None


def get_tg_tag(user_id):
    try:
        user = get_tg_user(user_id)
        if user is None:
            return '[ошибка]'
        if user.username is not None:
            return f'@{user.username}'
        else:
            return f'<a href="tg://user?id={user.id}">{user.full_name}</a>'
    except:
        return 'error'


def get_player_tag(player: PlayerInfo, platform: str) -> str:
    if platform != player.platform:
        return player.platform
    if platform == 'Telegram':
        return get_tg_tag(player.id)
    if platform == 'VK':
        return f'@id{player.id}'
    return 'error'
