import json
import os.path
import shutil


def delete_session(session_dir: str):
    if os.path.exists(session_dir):
        shutil.rmtree(session_dir)
        return "Действие отменено"
    else:
        return "Вы и так ничего не делали"


def create_session(session_dir: str, session_type: str):
    if os.path.exists(session_dir):
        shutil.rmtree(session_dir)

    session = dict()
    session["type"] = session_type
    session["stage"] = "initial"

    os.makedirs(session_dir)
    f = open(f'{session_dir}/session.json', mode='w')
    json.dump(session, f, indent=4)
    f.close()
    return session


def save_session(session_dir: str, session: dict):
    f = open(f'{session_dir}/session.json', mode='w')
    json.dump(session, f, indent=4)
    f.close()


def get_session(session_dir: str, session_type: str = None):
    session = None
    if not os.path.exists(session_dir) and session_type:
        session = create_session(session_dir, session_type)

    if not session:
        f = open(f'{session_dir}/session.json')
        session = json.load(f)

    return session
