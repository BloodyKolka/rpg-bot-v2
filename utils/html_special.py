def replace_html_special(text: str) -> str:
    text = text.replace('<', '&lt;')
    text = text.replace('>', '&gt;')
    text = text.replace('&', '&amp;')
    return text


def restore_html_special(text: str) -> str:
    text.replace('&lt;', '<')
    text = text.replace('&gt;', '>')
    text = text.replace('&amp;', '&')
    text = text.replace('<code>', '`')
    text = text.replace('</code>', '`')
    return text
