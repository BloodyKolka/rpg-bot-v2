import random
import re

from Equation import Expression


def roll_dice(sides, count, kh=0, kl=0):
    rolls = []
    roll_str = ''
    for i in range(count):
        roll = random.randint(1, sides)
        rolls.append(roll)
        roll_str += f'{roll}, '

    if kh + kl >= count:
        kh = 0
        kl = 0

    if kh != 0 or kl != 0:
        new_rolls = []
        rolls.sort()
        if kh != 0:
            high_rolls = rolls[-kh:]
            new_rolls += high_rolls
        if kl != 0:
            low_rolls = rolls[0:kl]
            new_rolls += low_rolls
        rolls = new_rolls

    return sum(rolls), roll_str


def create_message(name):
    return f'{name} кидает:\n'


dice_regexp = '([1-9][0-9]*)?d([1-9][0-9]*)'
keep_highest_regexp = 'kh([1-9]([0-9]*))'
keep_lowest_regexp = 'kl([1-9]([0-9]*))'
full_roll_regexp = f'({dice_regexp})({keep_highest_regexp})?({keep_lowest_regexp})?'


def roll_standard(dice_string: str):
    copy_dice_string = dice_string
    roll_str = '('

    cur_match = re.search(full_roll_regexp, copy_dice_string)
    while cur_match is not None:
        die_str = re.search(dice_regexp, cur_match.group()).group()
        if die_str[0] == 'd':
            die_str = '1' + die_str

        keep_highest_match = re.search(keep_highest_regexp, cur_match.group())
        if keep_highest_match is not None:
            kh_str = keep_highest_match.group()
            kh = int(kh_str.replace('kh', ''))
        else:
            kh = 0

        keep_lowest_match = re.search(keep_lowest_regexp, cur_match.group())
        if keep_lowest_match is not None:
            kl_str = keep_lowest_match.group()
            kl = int(kl_str.replace('kl', ''))
        else:
            kl = 0

        count = int(die_str.split('d')[0])
        sides = int(die_str.split('d')[1])
        cur_roll, cur_roll_str = roll_dice(sides, count, kh, kl)
        roll_str += cur_roll_str
        copy_dice_string = re.sub(full_roll_regexp, f'{cur_roll}', copy_dice_string, 1)
        cur_match = re.search(full_roll_regexp, copy_dice_string)

    roll_str += ')'
    roll_str = roll_str.replace(', )', ')')

    try:
        fn = Expression(copy_dice_string)
        return f'[{dice_string}]: {fn()}\n{roll_str}'
    except:
        return 'Ошибка в строке броска костей'


def roll_fate():
    results = [[1, '[+]'], [-1, '[-]'], [0, '[ ]']]
    res_str = ''
    res_num = 0
    for i in range(4):
        cur_res = random.choice(results)
        res_num += cur_res[0]
        res_str += cur_res[1]

    return f'[FATE]: {res_str} ({res_num})'


def roll_v5(pool, hunger):
    if pool <= hunger:
        hunger_dice = pool
        regular_dice = 0
    else:
        hunger_dice = hunger
        regular_dice = pool - hunger

    roll_str = '('

    successes = 0
    crits = 0
    if regular_dice > 0:
        roll_str += 'обычные кости: '
    for i in range(regular_dice):
        roll = random.randint(1, 10)
        if roll >= 6:
            successes += 1
            if roll == 10:
                crits += 1
        roll_str += f'{roll}, '

    beastial_fail_str = ''
    blood_crit_str = ''
    if hunger_dice > 0:
        roll_str += 'кровавые кости: '
    for i in range(hunger_dice):
        roll = random.randint(1, 10)
        if roll >= 6:
            successes += 1
            if roll == 10:
                crits += 1
                if crits >= 2:
                    blood_crit_str = '\nВозможность кровавого триумфа'
        if roll == 1:
            beastial_fail_str = '\nВозможность кровавого провала'
        roll_str += f'{roll}, '

    roll_str += ')'
    roll_str = roll_str.replace(', )', ')')
    roll_str = roll_str.replace(', кровавые кости:', '; кровавые кости:')

    successes = (successes - (crits // 2) * 2) + (crits // 2) * 4
    return f'[V5 пул: {pool}, голод: {hunger}]: {successes} успех(а/ов)\n{roll_str}{blood_crit_str}{beastial_fail_str}'
