from datetime import datetime


def get_from_file(filename: str):
    f = open(filename, 'r', encoding='utf-8')
    val = f.read()
    f.close()
    return val


def save_to_file(filename: str, val):
    f = open(filename, 'w', encoding='utf-8')
    f.write(str(val))
    f.close()


def save_exception(subfolder, e: Exception):
    filename = './data/exceptions/' + subfolder + datetime.now().isoformat()
    save_to_file(filename, str(e))
