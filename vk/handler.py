import json
import os

from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id

from action_handlers.add_game import handle_add_game_action
from action_handlers.add_player import handle_add_player_action
from action_handlers.change_player_name import handle_change_player_name_action
from action_handlers.delete_game import handle_delete_game_action
from action_handlers.edit_game import handle_edit_game_action
from action_handlers.kick_player import handle_kick_player_action
from action_handlers.message_players import handle_message_players_action

import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo

from menus.edit_game.vk.handler import handle_vk_edit_game_menu
from menus.game_info.vk.handler import handle_vk_game_info_menu
from menus.kick_player.vk.handler import handle_vk_kick_player_menu
from menus.list_games.vk.handler import handle_vk_list_games_menu
from menus.main.header import get_main_menu_header
from menus.main.vk.content import create_vk_main_menu
from menus.main.vk.handler import handle_vk_main_menu

from utils.file_utils import save_exception
from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.html_special import restore_html_special

from vk.vars import vk
from vk.vars import vk_group_chats
from vk.vars import vk_group_id
from vk.vars import vk_lock
from vk.vars import vk_session


def check_membership(user_id) -> bool:
    members = vk.groups.get_members(group_id=vk_group_id)
    if user_id in members["items"]:
        return True
    return False


def run_vk_bot():
    longpoll = VkBotLongPoll(vk_session, vk_group_id)
    while True:
        try:
            for event in longpoll.listen():
                if event.type == VkBotEventType.MESSAGE_NEW and event.message:
                    if event.message.is_cropped:
                        message = vk.messages.get_by_conversation_message_id(peer_id=f'{event.message.peer_id}', conversation_message_ids=f'{event.message.conversation_message_id}')["items"][0]
                    else:
                        message = event.message

                    user_id = event.message.from_id
                    msg_id = event.message.id

                    if not event.message.payload:
                        if event.from_chat:
                            if message["text"] == '/advertise':
                                with vk_lock:
                                    vk.messages.send(chat_id=event.chat_id, message=gdg.game_manager.create_advertisement('ЛС группы'), random_id=get_random_id())
                                continue
                            if event.chat.id not in vk_group_chats:
                                with vk_lock:
                                    vk.messages.send(chat_id=event.chat_id, message='Привет! Я работаю только в ЛС!', random_id=get_random_id())
                        else:
                            if not check_membership(user_id):
                                with vk_lock:
                                    vk.messages.send(user_id=user_id, message="Привет! Бот написан для небольшой группы людей, которые состоят в одном чате.", random_id=get_random_id())
                                continue

                            if message["text"] == '/del':
                                if 'reply_message' not in message:
                                    continue
                                msg_id = message["reply_message"]["conversation_message_id"]
                                try:
                                    with vk_lock:
                                        vk.messages.delete(peer_id=event.message.peer_id, cmids=f'{msg_id}',group_id=vk_group_id, delete_for_all=1)
                                except:
                                    pass
                                continue
                            elif message["text"] == '/start':
                                with vk_lock:
                                    vk.messages.send(user_id=user_id, message=get_main_menu_header(),
                                                     keyboard=create_vk_main_menu(msg_id), random_id=get_random_id())
                                continue
                            elif message["text"] == '/cancel':
                                with vk_lock:
                                    vk.messages.send(user_id=user_id, message=delete_session(f'./data/sessions/vk/{user_id}'), random_id=get_random_id())
                                continue

                            path = f'./data/sessions/vk/{user_id}'
                            if os.path.exists(path):
                                with vk_lock:
                                    users = vk.users.get(user_ids=f'{user_id}')
                                full_name = f'{users[0]["first_name"]} {users[0]["last_name"]}'

                                player = PlayerInfo(full_name, 'VK', user_id)
                                session = get_session(path)

                                reply = None
                                if session["type"] == 'add-game':
                                    reply = handle_add_game_action(message.text, path, player)
                                elif session["type"] == "message-players":
                                    reply = handle_message_players_action(message.text, path, player)
                                elif session["type"] == "delete-game":
                                    reply = handle_delete_game_action(message.text, path, player)
                                elif session["type"] == "edit-game":
                                    reply = handle_edit_game_action(message.text, path, player)
                                elif session["type"] == "change-player-name":
                                    reply = handle_change_player_name_action(message.text, path, player)
                                elif session["type"] == "kick-player":
                                    reply = handle_kick_player_action(message.text, path, player)
                                elif session["type"] == "add-player":
                                    reply = handle_add_player_action(message.text, path, player)

                                if reply:
                                    with vk_lock:
                                        vk.messages.send(user_id=user_id, message=restore_html_special(reply), random_id=get_random_id())
                    else:
                        with vk_lock:
                            users = vk.users.get(user_ids=f'{user_id}')
                        full_name = f'{users[0]["first_name"]} {users[0]["last_name"]}'
                        player = PlayerInfo(full_name, 'VK', user_id)

                        payload = json.loads(event.message.payload)
                        reply = None
                        if payload["menu"] == 'main':
                            reply = handle_vk_main_menu(payload, player, msg_id)
                        elif payload["menu"] == 'list-games':
                            reply = handle_vk_list_games_menu(payload, player, msg_id)
                        elif payload["menu"] == 'game-info':
                            reply = handle_vk_game_info_menu(payload, player, msg_id)
                        elif payload["menu"] == 'edit-game':
                            reply = handle_vk_edit_game_menu(payload, player, msg_id)
                        elif payload["menu"] == 'kick-player':
                            reply = handle_vk_kick_player_menu(payload, player, msg_id)
                        else:
                            reply = 'Внутренняя ошибка'
                        if reply:
                            with vk_lock:
                                vk.messages.send(user_id=user_id, message=restore_html_special(reply), random_id=get_random_id())
                        continue

        except Exception as e:
            save_exception('run_vk_bot', e)
            continue
        finally:
            continue
