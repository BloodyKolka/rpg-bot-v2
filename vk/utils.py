from vk.vars import vk
from vk.vars import vk_lock


def vk_delete_message(user_id: int, msg_id: int):
    try:
        with vk_lock:
            vk.messages.delete(user_id=user_id, message_ids=f'{msg_id}', delete_for_all=1)
    except:
        pass
