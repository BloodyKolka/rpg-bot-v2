from vk_api.utils import get_random_id

import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.edit_game.header import get_edit_game_header
from menus.edit_game.vk.content import create_vk_edit_game_menu
from menus.game_info.header import get_game_info_header
from menus.game_info.vk.content import create_vk_game_info_menu
from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.session_utils import save_session
from vk.utils import vk_delete_message
from vk.vars import vk
from vk.vars import vk_lock


def handle_vk_edit_game_menu(payload, player: PlayerInfo, msg_id: int):
    session_dir = f'./data/sessions/tg/{player.id}'
    session = get_session(session_dir, 'edit-game')
    session["stage"] = 'compose'
    game_id = payload["game_id"]

    vk_delete_message(player.id, payload["msg_id"])

    if payload["action"] == 'scroll':
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
            delete_session(session_dir)
            return result.desc
        game = result.res

        page = payload["page"]
        with vk_lock:
            vk.messages.send(peer_id=player.id, message=get_edit_game_header(),
                             keyboard=create_vk_edit_game_menu(game, msg_id, page), random_id=get_random_id())
    if payload["action"] == 'system-short':
        session["game_id"] = game_id
        session["parameter"] = 'system_short'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое краткое название системы', random_id=get_random_id())
    elif payload["action"] == 'system-full':
        session["game_id"] = game_id
        session["parameter"] = 'system_full'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое полное название системы', random_id=get_random_id())
    elif payload["action"] == 'campaign-name':
        session["game_id"] = game_id
        session["parameter"] = 'campaign_name'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое название кампании', random_id=get_random_id())
    elif payload["action"] == 'campaign-desc':
        session["game_id"] = game_id
        session["parameter"] = 'campaign_desc'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое описание кампании', random_id=get_random_id())
    elif payload["action"] == 'scenario-name':
        session["game_id"] = game_id
        session["parameter"] = 'scenario_name'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое название сценария', random_id=get_random_id())
    elif payload["action"] == 'scenario-desc':
        session["game_id"] = game_id
        session["parameter"] = 'scenario_desc'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое описание сценария', random_id=get_random_id())
    elif payload["action"] == 'session':
        session["game_id"] = game_id
        session["parameter"] = 'session'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новый номер сессии', random_id=get_random_id())
    elif payload["action"] == 'slots':
        session["game_id"] = game_id
        session["parameter"] = 'slots'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое максимальное количество игроков', random_id=get_random_id())
    elif payload["action"] == 'date-time':
        session["game_id"] = game_id
        session["parameter"] = 'date_time'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новую дату и время', random_id=get_random_id())
    elif payload["action"] == 'place':
        session["game_id"] = game_id
        session["parameter"] = 'place'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите новое место встречи', random_id=get_random_id())
    elif payload["action"] == 'gm-name':
        session["game_id"] = game_id
        session["parameter"] = 'gm_name'
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Напишите, как мне Вас называть', random_id=get_random_id())
    elif payload["action"] == 'go-back':
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
            delete_session(session_dir)
            return result.desc
        game = result.res

        header = get_game_info_header(game, 'VK')
        menu = create_vk_game_info_menu(game, player, msg_id)
        with vk_lock:
            vk.messages.send(user_id=player.id, message=header, keyboard=menu, random_id=get_random_id())

        delete_session(session_dir)
        return str()

    save_session(session_dir, session)
    return str()

