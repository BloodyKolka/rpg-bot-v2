from vk_api.keyboard import VkKeyboard
from vk_api.keyboard import VkKeyboardColor

from game_data.game import Game


def create_vk_edit_game_menu(game: Game, msg_id: int, page=0):
    keyboard = VkKeyboard(inline=True)
    game_id = game.game_id

    prev_page = 0
    next_page = 0
    if (page < 0) or (page > 2):
        page = 0

    if page == 0:
        keyboard.add_button("Краткое название системы", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "system-short", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        keyboard.add_button("Полное название системы", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "system-full", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        if not game.campaign_info.is_oneshot:
            keyboard.add_button("Название кампании", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "campaign-name", "game_id": game_id, "msg_id": msg_id + 1})
            keyboard.add_line()
            keyboard.add_button("Описание кампании", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "campaign-desc", "game_id": game_id, "msg_id": msg_id + 1})
            keyboard.add_line()
        prev_page = 2
        next_page = 1
    elif page == 1:
        keyboard.add_button("Название сценария", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "scenario-name", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        keyboard.add_button("Описание сценария", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "scenario-desc", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        keyboard.add_button("Номер сессии", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "session", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        keyboard.add_button("Количество мест", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "slots", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        prev_page = 0
        next_page = 2
    elif page == 2:
        keyboard.add_button("Дата и время", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "date-time", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        keyboard.add_button("Место", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "open", "place": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        keyboard.add_button("Имя ведущего", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "gm-name", "game_id": game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
        prev_page = 1
        next_page = 0

    keyboard.add_button("<--", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "scroll", "page": prev_page, "game_id": game_id, "msg_id": msg_id + 1})
    keyboard.add_button("-- Назад --", color=VkKeyboardColor.NEGATIVE, payload={"menu": "edit-game", "action": "go-back", "game_id": game_id, "msg_id": msg_id + 1})
    keyboard.add_button("-->", color=VkKeyboardColor.SECONDARY, payload={"menu": "edit-game", "action": "scroll", "page": next_page, "game_id": game_id, "msg_id": msg_id + 1})

    return keyboard.get_keyboard()
