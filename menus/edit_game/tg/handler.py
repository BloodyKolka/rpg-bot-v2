import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.game_info.header import get_game_info_header
from menus.game_info.tg.content import create_tg_game_info_menu
from tg.vars import tg_bot
from tg.vars import tg_lock
from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.session_utils import save_session


def handle_tg_edit_game_menu(command, chat_id, message_id, user):
    session_dir = f'./data/sessions/tg/{user.id}'
    session = get_session(session_dir, 'edit-game')
    session["stage"] = 'compose'

    if command.startswith('system-short_'):
        game_id = command.replace('system-short_', '')
        session["game_id"] = game_id
        session["parameter"] = 'system_short'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое краткое название системы', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('system-full_'):
        game_id = command.replace('system-full_', '')
        session["game_id"] = game_id
        session["parameter"] = 'system_full'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое полное название системы', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('campaign-name_'):
        game_id = command.replace('campaign-name_', '')
        session["game_id"] = game_id
        session["parameter"] = 'campaign_name'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое название кампании', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('campaign-desc_'):
        game_id = command.replace('campaign-desc_', '')
        session["game_id"] = game_id
        session["parameter"] = 'campaign_desc'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое описание кампании', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('scenario-name_'):
        game_id = command.replace('scenario-name_', '')
        session["game_id"] = game_id
        session["parameter"] = 'scenario_name'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое название сценария', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('scenario-desc_'):
        game_id = command.replace('scenario-desc_', '')
        session["game_id"] = game_id
        session["parameter"] = 'scenario_desc'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое описание сценария', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('session_'):
        game_id = command.replace('session_', '')
        session["game_id"] = game_id
        session["parameter"] = 'session'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новый номер сессии', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('slots_'):
        game_id = command.replace('slots_', '')
        session["game_id"] = game_id
        session["parameter"] = 'slots'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое максимальное количество игроков', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('date-time_'):
        game_id = command.replace('date-time_', '')
        session["game_id"] = game_id
        session["parameter"] = 'date_time'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новую дату и время', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('place_'):
        game_id = command.replace('place_', '')
        session["game_id"] = game_id
        session["parameter"] = 'place'
        with tg_lock:
            tg_bot.edit_message_text('Напишите новое место встречи', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('gm-name_'):
        game_id = command.replace('gm-name_', '')
        session["game_id"] = game_id
        session["parameter"] = 'gm_name'
        with tg_lock:
            tg_bot.edit_message_text('Напишите, как мне Вас называть', chat_id, message_id, parse_mode='HTML')
    elif command.startswith('go-back_'):
        game_id = int(command.replace('go-back_', ''))
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            delete_session(session_dir)
            return result.desc
        game = result.res

        player = PlayerInfo(user.full_name, 'Telegram', user.id)

        header = get_game_info_header(game, 'Telegram')
        menu = create_tg_game_info_menu(game, player)
        with tg_lock:
            tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
            tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)

        delete_session(session_dir)
        return str()

    save_session(session_dir, session)
    return str()

