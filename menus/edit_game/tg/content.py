from telebot import types

from game_data.game import Game


def create_tg_edit_game_menu(game: Game):
    keyboard = []

    game_id = game.game_id

    keyboard.append([types.InlineKeyboardButton(text='Краткое название системы', callback_data=f'edit-game_system-short_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Полное название системы', callback_data=f'edit-game_system-full_{game_id}')])
    if not game.campaign_info.is_oneshot:
        keyboard.append([types.InlineKeyboardButton(text='Название кампании', callback_data=f'edit-game_campaign-name_{game_id}')])
        keyboard.append([types.InlineKeyboardButton(text='Описание кампании', callback_data=f'edit-game_campaign-desc_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Название сценария', callback_data=f'edit-game_scenario-name_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Описание сценария', callback_data=f'edit-game_scenario-desc_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Номер сессии', callback_data=f'edit-game_session_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Количество мест', callback_data=f'edit-game_slots_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Дата и время игры', callback_data=f'edit-game_date-time_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Место игры', callback_data=f'edit-game_place_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Имя ведущего', callback_data=f'edit-game_gm-name_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'edit-game_go-back_{game_id}')])

    return types.InlineKeyboardMarkup(keyboard)
