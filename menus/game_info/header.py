import json

from game_data.game import Game
from game_data.player_info import PlayerInfo

from utils.get_player_tag import get_player_tag
from utils.html_special import replace_html_special


def get_game_info_header(game: Game, platform: str):
    if game.registration_open:
        status = 'ОТКР.'
    else:
        status = 'ЗАКР.'

    if game.campaign_info.is_oneshot:
        name = f'Ваншот - {game.session_info.name}'
    else:
        name = f'{game.campaign_info.name} (сессия {game.campaign_info.session})'

    game_info = replace_html_special(f'[{game.system_info.short_name}][{status}] {name}\n')
    game_info += replace_html_special(f' - Сессия: {game.session_info.name}\n')
    game_info += replace_html_special(f' - Система: {game.system_info.full_name}\n')
    game_info += replace_html_special(f' - Ведущий: {game.gm_info.name}') + f' ({get_player_tag(game.gm_info, platform)})\n'
    game_info += replace_html_special(f' - Дата и время: {game.date_time}\n')
    game_info += replace_html_special(f' - Место: {game.place}\n')
    game_info += replace_html_special(f' - Свободные слоты: {game.free_slots}/{game.total_slots}\n')
    game_info += replace_html_special(f' - Описание сессии:\n')
    game_info += replace_html_special(f'{game.session_info.desc}\n')

    if not game.campaign_info.is_oneshot:
        game_info += replace_html_special(f' - Описание кампании:\n')
        game_info += replace_html_special(f'{game.campaign_info.desc}\n')
    game_info += replace_html_special(f' - Список записавшихся:\n')

    for player in game.players:
        if player.id != -1:
            game_info += replace_html_special(f'{player.name}') + f' ({get_player_tag(player, platform)})\n'
        else:
            try:
                adding_player = PlayerInfo.from_json(json.loads(player.platform))
                game_info += replace_html_special(f'{player.name} (+1 от {adding_player.name}') + f' ({get_player_tag(adding_player, platform)}))\n'
            except:
                game_info += replace_html_special(f'{player.name} (+1)\n')

    if not game.players:
        game_info += replace_html_special('Пока никто не записался')

    return game_info
