from vk_api.utils import get_random_id

import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.edit_game.header import get_edit_game_header
from menus.edit_game.vk.content import create_vk_edit_game_menu
from menus.kick_player.header import get_kick_player_header
from menus.kick_player.vk.content import create_vk_kick_player_menu
from menus.list_games.header import get_list_games_header
from menus.list_games.vk.content import create_vk_list_games_menu
from utils.get_player_tag import get_player_tag
from utils.send_message import send_message
from utils.session_utils import create_session
from utils.session_utils import save_session
from vk.utils import vk_delete_message
from vk.vars import vk
from vk.vars import vk_lock


def __go_back(player_id, msg_id):
    header = get_list_games_header()
    menu = create_vk_list_games_menu(msg_id + 1)
    with vk_lock:
        vk.messages.send(user_id=player_id, message=header, keyboard=menu, random_id=get_random_id())


def handle_vk_game_info_menu(payload, player: PlayerInfo, msg_id: int):
    session_dir = f'./data/sessions/vk/{player.id}'

    vk_delete_message(player.id, payload["msg_id"])

    if payload["action"] == 'go-back':
        __go_back(player.id, payload["msg_id"])
        return
    
    game_id = payload["game_id"]
    
    if payload["action"] == 'message-players':
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Хорошо! Напишите, что Вы хотите сообщить игрокам', random_id=get_random_id())

        session = create_session(session_dir, 'message-players')
        session["game_id"] = game_id
        session["stage"] = "compose"
        save_session(session_dir, session)
    elif payload["action"] == 'change-player-name':
        with vk_lock:
            vk.messages.send(user_id=player.id, message="Хорошо! Напишите, как Вы хотите, чтобы я Вас подписывал", random_id=get_random_id())

        session = create_session(session_dir, 'change-player-name')
        session["game_id"] = game_id
        session["stage"] = "compose"
        save_session(session_dir, session)
    elif payload["action"] == 'open-reg':
        result = gdg.game_manager.get_game(int(game_id))

        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        game = result.res

        game.registration_open = True
        gdg.game_manager.update_game(game)

        __go_back(player.id, payload["msg_id"])
        return "Регистрация открыта!"
    elif payload["action"] == 'close-reg':
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        game = result.res

        game.registration_open = False
        gdg.game_manager.update_game(game)

        __go_back(player.id, payload["msg_id"])
        return "Регистрация закрыта!"
    elif payload["action"] == 'bump-session':
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        game = result.res

        game.campaign_info.session += 1
        gdg.game_manager.update_game(game)

        __go_back(player.id, payload["msg_id"])
        return "Номер сессии увеличен!"
    elif payload["action"] == 'edit-game':
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        game = result.res

        header = get_edit_game_header()
        menu = create_vk_edit_game_menu(game, msg_id)
        with vk_lock:
            vk.messages.send(user_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
        return str()
    elif payload["action"] == 'delete-game':
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Хорошо! Напишите "Подтверждаю", если Вы уверены. Если Вы напишите что-то другое - удаление будет отменено.', random_id=get_random_id())

        session = create_session(session_dir, 'delete-game')
        session["game_id"] = game_id
        session["stage"] = "confirm"
        save_session(session_dir, session)

        return str()
    elif payload["action"] == 'register':
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        game = result.res

        result = game.register(player)
        if not result.ok:
            __go_back(player.id, payload["msg_id"])
            return result.desc
        result = gdg.game_manager.update_game(game)
        if not result.ok:
            __go_back(player.id, payload["msg_id"])
            return result.desc

        gm_msg = f"Привет, обновление по Вашей игре\n{game.get_header()}\n"

        with vk_lock:
            users = vk.users.get(user_ids=f'{player.id}')
        full_name = f'{users[0]["first_name"]} {users[0]["last_name"]}'

        gm_msg += f'На Вашу игру записался {full_name} ({get_player_tag(player, game.gm_info.platform)})'

        if game.free_slots == 0:
            gm_msg += '\nТеперь на неё записано максимальное число игроков'

        send_message(gm_msg, game.gm_info)
        __go_back(player.id, payload["msg_id"])
        return 'Вы успешно записались на игру!'
    elif payload["action"] == 'unregister':
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        game = result.res

        result = game.unregister(player)
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        result = gdg.game_manager.update_game(game)
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return

        gm_msg = f"Привет, обновление по Вашей игре\n{game.get_header()}\n"

        with vk_lock:
            users = vk.users.get(user_ids=f'{player.id}')
        full_name = f'{users[0]["first_name"]} {users[0]["last_name"]}'

        gm_msg += f'{full_name} ({get_player_tag(player, game.gm_info.platform)} отменил(а) запись на Вашу игру'

        send_message(gm_msg, game.gm_info)

        __go_back(player.id, payload["msg_id"])
        return 'Вы успешно отменили запись на игру!'
    elif payload["action"] == 'add-player':
        session = create_session(session_dir, 'add-player')
        session["game_id"] = game_id
        session["stage"] = "compose"
        save_session(session_dir, session)

        with vk_lock:
            vk.messages.send(user_id=player.id, message='Хорошо, напишите, как мне его записать', random_id=get_random_id())
    elif payload["action"] == 'kick-player':
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
                __go_back(player.id, payload["msg_id"])
            return
        game = result.res

        header = get_kick_player_header()
        menu = create_vk_kick_player_menu(game, msg_id)
        with vk_lock:
            vk.messages.send(user_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
