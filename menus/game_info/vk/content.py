from vk_api.keyboard import VkKeyboard
from vk_api.keyboard import VkKeyboardColor

from game_data.game import Game
from game_data.player_info import PlayerInfo
from game_data.player_info import is_same_user
from game_data.player_info import is_user_in_list


def create_vk_game_info_menu(game: Game, player: PlayerInfo, msg_id: int):
    keyboard = VkKeyboard(inline=True)

    if is_same_user(game.gm_info, player):
        keyboard.add_button('Сообщение игрокам', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "message-players", "game_id": game.game_id, "msg_id": msg_id + 1})
        keyboard.add_line()

        if game.registration_open:
            keyboard.add_button('Закрыть регистрацию', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "close-reg", "game_id": game.game_id, "msg_id": msg_id + 1})
        else:
            keyboard.add_button('Открыть регистрацию', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "open-reg", "game_id": game.game_id, "msg_id": msg_id + 1})
        keyboard.add_line()

        keyboard.add_button('Увеличить номер сессии', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "bump-session", "game_id": game.game_id, "msg_id": msg_id + 1})
        keyboard.add_line()

        if game.free_slots > 0:
            keyboard.add_button('Добавить игрока +1', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "add-player", "game_id": game.game_id, "msg_id": msg_id + 1})
        if game.free_slots < game.total_slots:
            keyboard.add_button('Выгнать игрока', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "kick-player", "game_id": game.game_id, "msg_id": msg_id + 1})
        keyboard.add_line()

        keyboard.add_button('Редактировать', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "edit-game", "game_id": game.game_id, "msg_id": msg_id + 1})
        keyboard.add_button('Удалить', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "delete-game", "game_id": game.game_id, "msg_id": msg_id + 1})
        keyboard.add_line()
    else:
        if game.registration_open:
            if is_user_in_list(player, game.players):
                keyboard.add_button('Поменять имя', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "change-player-name", "game_id": game.game_id, "msg_id": msg_id + 1})
                keyboard.add_line()
                if game.free_slots > 0:
                    keyboard.add_button('Добавить игрока +1', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "add-player", "game_id": game.game_id, "msg_id": msg_id + 1})
                    keyboard.add_line()
                keyboard.add_button('Отменить регистрацию', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "unregister", "game_id": game.game_id, "msg_id": msg_id + 1})
                keyboard.add_line()
            else:
                if game.free_slots > 0:
                    keyboard.add_button('Зарегистрироваться', color=VkKeyboardColor.SECONDARY, payload={"menu": "game-info", "action": "register", "game_id": game.game_id, "msg_id": msg_id + 1})
                    keyboard.add_line()

    keyboard.add_button('- Назад -', color=VkKeyboardColor.NEGATIVE, payload={"menu": "game-info", "action": "go-back", "msg_id": msg_id + 1})

    return keyboard.get_keyboard()
