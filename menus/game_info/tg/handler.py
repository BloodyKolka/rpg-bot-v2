import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.edit_game.header import get_edit_game_header
from menus.edit_game.tg.content import create_tg_edit_game_menu
from menus.kick_player.header import get_kick_player_header
from menus.kick_player.tg.content import create_tg_kick_player_menu
from menus.list_games.header import get_list_games_header
from menus.list_games.tg.content import create_tg_list_games_menu
from tg.vars import tg_bot
from tg.vars import tg_lock
from utils.get_player_tag import get_player_tag, get_tg_user
from utils.send_message import send_message
from utils.session_utils import create_session
from utils.session_utils import save_session


def __go_back(chat_id, message_id):
    header = get_list_games_header()
    menu = create_tg_list_games_menu()
    with tg_lock:
        tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)


def handle_tg_game_info_menu(command, chat_id, message_id, user):
    session_dir = f'./data/sessions/tg/{user.id}'

    if command == 'go-back':
        __go_back(chat_id, message_id)
        return str()
    elif command.startswith('message-players_'):
        game_id = command.replace('message-players_', '')

        with tg_lock:
            tg_bot.edit_message_text("Хорошо! Напишите, что Вы хотите сообщить игрокам (можете отправить опрос, если хотите собрать информацию)", chat_id=chat_id,
                                     message_id=message_id, parse_mode='HTML')

        session = create_session(session_dir, 'message-players')
        session["game_id"] = game_id
        session["stage"] = "compose"
        save_session(session_dir, session)

        return str()
    elif command.startswith('change-player-name_'):
        game_id = command.replace('change-player-name_', '')

        with tg_lock:
            tg_bot.edit_message_text("Хорошо! Напишите, как Вы хотите, чтобы я Вас подписывал", chat_id=chat_id,
                                     message_id=message_id, parse_mode='HTML')

        session = create_session(session_dir, 'change-player-name')
        session["game_id"] = game_id
        session["stage"] = "compose"
        save_session(session_dir, session)

        return str()
    elif command.startswith('open-reg_'):
        game_id = command.replace('open-reg_', '')
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        game = result.res

        game.registration_open = True
        gdg.game_manager.update_game(game)

        __go_back(chat_id, message_id)
        return "Регистрация открыта!"
    elif command.startswith('close-reg_'):
        game_id = command.replace('close-reg_', '')
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        game = result.res

        game.registration_open = False
        gdg.game_manager.update_game(game)

        __go_back(chat_id, message_id)
        return "Регистрация закрыта!"
    elif command.startswith('bump-session_'):
        game_id = command.replace('bump-session_', '')
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        game = result.res

        game.campaign_info.session += 1
        gdg.game_manager.update_game(game)

        __go_back(chat_id, message_id)
        return "Номер сессии увеличен!"
    elif command.startswith('edit-game_'):
        game_id = command.replace('edit-game_', '')
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        game = result.res

        header = get_edit_game_header()
        menu = create_tg_edit_game_menu(game)
        with tg_lock:
            tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
            tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)
        return str()
    elif command.startswith('delete-game_'):
        game_id = command.replace('delete-game_', '')

        with tg_lock:
            tg_bot.edit_message_text('Хорошо! Напишите <code>Подтверждаю</code>, если Вы уверены. Если Вы напишите что-то другое - удаление будет отменено.', chat_id=chat_id,
                                     message_id=message_id, parse_mode='HTML')

        session = create_session(session_dir, 'delete-game')
        session["game_id"] = game_id
        session["stage"] = "confirm"
        save_session(session_dir, session)

        return str()
    elif command.startswith('register_'):
        game_id = command.replace('register_', '')
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        game = result.res

        player = PlayerInfo(user.full_name, 'Telegram', user.id)
        result = game.register(player)
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        result = gdg.game_manager.update_game(game)
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc

        gm_msg = f"Привет, обновление по Вашей игре\n{game.get_header()}\n"

        player_user = get_tg_user(user.id)
        gm_msg += f'На Вашу игру записался {player_user.full_name} ({get_player_tag(player, game.gm_info.platform)}'

        if game.free_slots == 0:
            gm_msg += '\nТеперь на неё записано максимальное число игроков'

        send_message(gm_msg, game.gm_info)
        __go_back(chat_id, message_id)
        return 'Вы успешно записались на игру!'
    elif command.startswith('unregister_'):
        game_id = command.replace('unregister_', '')
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        game = result.res

        player = PlayerInfo(user.full_name, 'Telegram', user.id)
        result = game.unregister(player)
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        result = gdg.game_manager.update_game(game)
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc

        gm_msg = f"Привет, обновление по Вашей игре\n{game.get_header()}\n"

        player_user = get_tg_user(user.id)
        gm_msg += f'{player_user.full_name} ({get_player_tag(player, game.gm_info.platform)} отменил(а) запись на Вашу игру'

        send_message(gm_msg, game.gm_info)

        __go_back(chat_id, message_id)
        return 'Вы успешно отменили запись на игру!'
    elif command.startswith('add-player_'):
        game_id = command.replace('add-player_', '')

        session = create_session(session_dir, 'add-player')
        session["game_id"] = game_id
        session["stage"] = "compose"
        save_session(session_dir, session)

        with tg_lock:
            tg_bot.edit_message_text('Хорошо, напишите, как мне его записать?', chat_id=chat_id, message_id=message_id, parse_mode='HTML')

        return str()
    elif command.startswith('kick-player_'):
        game_id = command.replace('kick-player_', '')
        result = gdg.game_manager.get_game(int(game_id))
        if not result.ok:
            __go_back(chat_id, message_id)
            return result.desc
        game = result.res

        header = get_kick_player_header()
        menu = create_tg_kick_player_menu(game)
        with tg_lock:
            tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
            tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)

        return str()
