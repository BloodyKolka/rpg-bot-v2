from telebot import types

from game_data.game import Game
from game_data.player_info import PlayerInfo
from game_data.player_info import is_same_user
from game_data.player_info import is_user_in_list


def create_tg_game_info_menu(game: Game, player: PlayerInfo):
    keyboard = []

    if is_same_user(game.gm_info, player):
        keyboard.append([types.InlineKeyboardButton(text='Сообщение игрокам', callback_data=f'game-info_message-players_{game.game_id}')])
        if game.registration_open:
            keyboard.append([types.InlineKeyboardButton(text='Закрыть регистрацию', callback_data=f'game-info_close-reg_{game.game_id}')])
        else:
            keyboard.append([types.InlineKeyboardButton(text='Открыть регистрацию', callback_data=f'game-info_open-reg_{game.game_id}')])
        keyboard.append([types.InlineKeyboardButton(text='Увеличить номер сессии', callback_data=f'game-info_bump-session_{game.game_id}')])
        if game.free_slots > 0:
            keyboard.append([types.InlineKeyboardButton(text='Добавить игрока +1', callback_data=f'game-info_add-player_{game.game_id}')])
        if game.free_slots < game.total_slots:
            keyboard.append([types.InlineKeyboardButton(text='Выгнать игрока', callback_data=f'game-info_kick-player_{game.game_id}')])
        keyboard.append([types.InlineKeyboardButton(text='Редактировать', callback_data=f'game-info_edit-game_{game.game_id}')])
        keyboard.append([types.InlineKeyboardButton(text='Удалить', callback_data=f'game-info_delete-game_{game.game_id}')])
    else:
        if game.registration_open:
            if is_user_in_list(player, game.players):
                keyboard.append([types.InlineKeyboardButton(text='Поменять имя', callback_data=f'game-info_change-player-name_{game.game_id}')])
                if game.free_slots > 0:
                    keyboard.append([types.InlineKeyboardButton(text='Добавить игрока +1', callback_data=f'game-info_add-player_{game.game_id}')])
                keyboard.append([types.InlineKeyboardButton(text='Отменить регистрацию', callback_data=f'game-info_unregister_{game.game_id}')])
            else:
                if game.free_slots > 0:
                    keyboard.append([types.InlineKeyboardButton(text='Зарегистрироваться', callback_data=f'game-info_register_{game.game_id}')])

    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'game-info_go-back')])

    return types.InlineKeyboardMarkup(keyboard)
