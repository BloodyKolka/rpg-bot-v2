import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.game_info.header import get_game_info_header
from menus.game_info.tg.content import create_tg_game_info_menu
from tg.vars import tg_bot
from tg.vars import tg_lock
from utils.session_utils import create_session
from utils.session_utils import save_session


def handle_tg_kick_player_menu(command, chat_id, message_id, user):
    if command.startswith('go-back_'):
        game_id = int(command.replace('go-back_', ''))
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            return result.desc
        game = result.res
        player = PlayerInfo(user.full_name, 'Telegram', user.id)

        header = get_game_info_header(game, 'Telegram')
        menu = create_tg_game_info_menu(game, player)
        with tg_lock:
            tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
            tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)
        return str()
    elif command.startswith('kick_'):
        game_id = command.replace('kick_', '').split('_')[0]
        player_num = command.replace('kick_', '').split('_')[1]

        path = f'./data/sessions/tg/{user.id}'
        session = create_session(path, 'kick-player')
        session["type"] = "kick-player"
        session["game_id"] = game_id
        session["player_num"] = player_num
        save_session(path, session)

        with tg_lock:
            tg_bot.edit_message_text('Хорошо! Напишите <code>Подтверждаю</code>, если Вы уверены. Если напишете что-то другое - действие будет отменено.', chat_id=chat_id, message_id=message_id, parse_mode='HTML')
        return str()
