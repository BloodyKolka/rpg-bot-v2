from telebot import types

from game_data.game import Game


def create_tg_kick_player_menu(game: Game):
    keyboard = []

    for i in range(len(game.players)):
        keyboard.append([types.InlineKeyboardButton(text=f'{game.players[i].name}',
                                                    callback_data=f'kick-player_kick_{game.game_id}_{i}')])
    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'kick-player_go-back_{game.game_id}')])

    return types.InlineKeyboardMarkup(keyboard)
