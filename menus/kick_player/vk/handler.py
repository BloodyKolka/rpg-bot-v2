from vk_api.utils import get_random_id

import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.game_info.header import get_game_info_header
from menus.game_info.vk.content import create_vk_game_info_menu
from menus.kick_player.header import get_kick_player_header
from menus.kick_player.vk.content import create_vk_kick_player_menu
from utils.session_utils import create_session
from utils.session_utils import save_session
from vk.utils import vk_delete_message
from vk.vars import vk
from vk.vars import vk_lock


def handle_vk_kick_player_menu(payload, player: PlayerInfo, msg_id: int):
    game_id = payload["game_id"]
    result = gdg.game_manager.get_game(game_id)
    if not result.ok:
        with vk_lock:
            vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
        return
    game = result.res

    vk_delete_message(player.id, payload["msg_id"])

    if payload["action"] == 'go-back':
        header = get_game_info_header(game, 'VK')
        menu = create_vk_game_info_menu(game, player, msg_id)
        with vk_lock:
            vk.messages.send(peer_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
    elif payload["action"] == 'scroll':
        page = payload["page"]
        header = get_kick_player_header()
        menu = create_vk_kick_player_menu(game, msg_id, page)
        with vk_lock:
            vk.messages.send(peer_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
    elif payload["action"] == 'kick':
        player_num = payload["player_num"]

        path = f'./data/sessions/vk/{player.id}'
        session = create_session(path, 'kick-player')
        session["type"] = "kick-player"
        session["game_id"] = game_id
        session["player_num"] = player_num
        save_session(path, session)

        with vk_lock:
            vk.messages.send(peer_id=player.id, message='Хорошо! Напишите "Подтверждаю", если Вы уверены. Если напишете что-то другое - действие будет отменено', random_id=get_random_id())
