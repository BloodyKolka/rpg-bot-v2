import math

from vk_api.keyboard import VkKeyboard
from vk_api.keyboard import VkKeyboardColor

from game_data.game import Game


def create_vk_kick_player_menu(game: Game, msg_id, page=0):
    MAX_BUTTON_COUNT = 6

    keyboard = VkKeyboard(inline=True)

    players = game.players
    player_count = len(players)
    if page * (MAX_BUTTON_COUNT - 1) >= player_count:
        page = 0

    if (page + 1) * (MAX_BUTTON_COUNT - 1) >= player_count:
        next_page = 0
    else:
        next_page = page + 1

    if page == 0:
        prev_page = math.trunc(player_count / (MAX_BUTTON_COUNT - 1) - 0.1)
    else:
        prev_page = page - 1

    for i in range(page * (MAX_BUTTON_COUNT - 1), min(page * (MAX_BUTTON_COUNT - 1) + 5, player_count)):
        name = players[i].name
        if len(name) > 40:
            name = name[0: 37]
            name += '...'

        keyboard.add_button(name, color=VkKeyboardColor.SECONDARY,
                            payload={"menu": "kick-player", "action": "kick", "game_id": game.game_id, "player_num": i,
                                     "msg_id": msg_id + 1})
        keyboard.add_line()

    if player_count > MAX_BUTTON_COUNT - 1:
        keyboard.add_button("<--", color=VkKeyboardColor.SECONDARY,
                            payload={"menu": "kick-player", "action": "scroll", "game_id": game.game_id, "page": prev_page, "msg_id": msg_id + 1})
    keyboard.add_button("-- Назад --", color=VkKeyboardColor.NEGATIVE,
                        payload={"menu": "kick-player", "action": "go-back", "game_id": game.game_id, "msg_id": msg_id + 1})
    if player_count > MAX_BUTTON_COUNT - 1:
        keyboard.add_button("-->", color=VkKeyboardColor.SECONDARY,
                            payload={"menu": "kick-player", "action": "scroll", "game_id": game.game_id, "page": next_page, "msg_id": msg_id + 1})

    return keyboard.get_keyboard()
