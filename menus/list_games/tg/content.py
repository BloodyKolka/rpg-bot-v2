from telebot import types

import game_data.global_vars as gdg


def create_tg_list_games_menu():
    keyboard = []

    for game in gdg.game_manager.list_games():
        keyboard.append(
            [types.InlineKeyboardButton(text=game.get_header(), callback_data=f'list-games_{game.game_id}')])

    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'list-games_go-back')])

    return types.InlineKeyboardMarkup(keyboard)
