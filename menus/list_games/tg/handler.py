import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.game_info.header import get_game_info_header
from menus.game_info.tg.content import create_tg_game_info_menu
from menus.main.header import get_main_menu_header
from menus.main.tg.content import create_tg_main_menu
from tg.vars import tg_bot
from tg.vars import tg_lock


def handle_tg_list_games_menu(command, chat_id, message_id, user):
    if command == 'go-back':
        header = get_main_menu_header()
        menu = create_tg_main_menu()
        with tg_lock:
            tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
            tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)
        return str()
    else:
        game_id = int(command)
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            return result.desc
        game = result.res
        player = PlayerInfo(user.full_name, 'Telegram', user.id)

        header = get_game_info_header(game, 'Telegram')
        menu = create_tg_game_info_menu(game, player)
        with tg_lock:
            tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
            tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)
        return str()
