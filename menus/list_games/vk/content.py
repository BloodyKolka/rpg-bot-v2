import math

from vk_api.keyboard import VkKeyboard
from vk_api.keyboard import VkKeyboardColor

import game_data.global_vars as gdg


def create_vk_list_games_menu(msg_id: int, page=0):
    MAX_BUTTON_COUNT = 6

    keyboard = VkKeyboard(inline=True)

    games = gdg.game_manager.list_games()
    games_count = len(games)
    if page * (MAX_BUTTON_COUNT - 1) >= games_count:
        page = 0

    if (page + 1) * (MAX_BUTTON_COUNT - 1) >= games_count:
        next_page = 0
    else:
        next_page = page + 1

    if page == 0:
        prev_page = math.trunc(games_count / (MAX_BUTTON_COUNT - 1) - 0.1)
    else:
        prev_page = page - 1

    for i in range(page * (MAX_BUTTON_COUNT - 1), min(page * (MAX_BUTTON_COUNT - 1) + 5, games_count)):
        header = games[i].get_header()
        if len(header) > 40:
            header = header[0 : 37]
            header += '...'

        keyboard.add_button(header, color=VkKeyboardColor.SECONDARY,
                            payload={"menu": "list-games", "action": "open", "game_id": games[i].game_id, "msg_id": msg_id + 1})
        keyboard.add_line()

    if games_count > MAX_BUTTON_COUNT - 1:
        keyboard.add_button("<--", color=VkKeyboardColor.SECONDARY,
                            payload={"menu": "list-games", "action": "scroll", "page": prev_page, "msg_id": msg_id + 1})
    keyboard.add_button("-- Назад --", color=VkKeyboardColor.NEGATIVE,
                        payload={"menu": "list-games", "action": "go-back", "msg_id": msg_id + 1})
    if games_count > MAX_BUTTON_COUNT - 1:
        keyboard.add_button("-->", color=VkKeyboardColor.SECONDARY,
                            payload={"menu": "list-games", "action": "scroll", "page": next_page, "msg_id": msg_id + 1})

    return keyboard.get_keyboard()
