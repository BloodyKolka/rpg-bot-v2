from vk_api.utils import get_random_id

import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from menus.game_info.header import get_game_info_header
from menus.game_info.vk.content import create_vk_game_info_menu
from menus.list_games.header import get_list_games_header
from menus.list_games.vk.content import create_vk_list_games_menu
from menus.main.header import get_main_menu_header
from menus.main.vk.content import create_vk_main_menu
from vk.utils import vk_delete_message
from vk.vars import vk
from vk.vars import vk_lock


def handle_vk_list_games_menu(payload, player: PlayerInfo, msg_id: int):
    vk_delete_message(player.id, payload["msg_id"])

    if payload["action"] == 'go-back':
        header = get_main_menu_header()
        menu = create_vk_main_menu(msg_id)
        with vk_lock:
            vk.messages.send(peer_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
    elif payload["action"] == 'scroll':
        page = payload["page"]
        header = get_list_games_header()
        menu = create_vk_list_games_menu(msg_id, page)
        with vk_lock:
            vk.messages.send(peer_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
    elif payload["action"] == 'open':
        game_id = payload["game_id"]
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            with vk_lock:
                vk.messages.send(peer_id=player.id, message=result.desc, random_id=get_random_id())
            return
        game = result.res

        header = get_game_info_header(game, 'VK')
        menu = create_vk_game_info_menu(game, player, msg_id)
        with vk_lock:
            vk.messages.send(peer_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
