from telebot import types


def create_tg_main_menu():
    keyboard = [
        [types.InlineKeyboardButton(text='Открыть список игр', callback_data='main_list-games')],
        [types.InlineKeyboardButton(text='Создать игру', callback_data='main_add-game')],
        [types.InlineKeyboardButton(text='Выйти', callback_data='main_exit')],
    ]

    return types.InlineKeyboardMarkup(keyboard)
