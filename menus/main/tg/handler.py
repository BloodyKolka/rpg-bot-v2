import telebot

from action_handlers.add_game import handle_add_game_action
from game_data.player_info import PlayerInfo
from menus.list_games.header import get_list_games_header
from menus.list_games.tg.content import create_tg_list_games_menu
from tg.vars import tg_bot
from tg.vars import tg_lock


def handle_tg_main_menu(command, chat_id, message_id, user: telebot.types.User):
    if command == 'list-games':
        header = get_list_games_header()
        menu = create_tg_list_games_menu()
        with tg_lock:
            tg_bot.edit_message_text(header, chat_id=chat_id, message_id=message_id, parse_mode='HTML')
            tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id, reply_markup=menu)
        return str()
    elif command == 'add-game':
        path = f'./data/sessions/tg/{user.id}'
        player = PlayerInfo(user.full_name, 'Telegram', user.id)
        with tg_lock:
            tg_bot.edit_message_text(handle_add_game_action('', path, player), chat_id=chat_id, message_id=message_id, parse_mode='HTML')
        return str()
    elif command == 'exit':
        with tg_lock:
            tg_bot.edit_message_text('Пока 👋! Если ещё понадоблюсь, введи /start', chat_id=chat_id, message_id=message_id, parse_mode='HTML')
        return str()
