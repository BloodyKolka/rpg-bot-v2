from vk_api.keyboard import VkKeyboard
from vk_api.keyboard import VkKeyboardColor


def create_vk_main_menu(msg_id: int):
    keyboard = VkKeyboard(inline=True)
    keyboard.add_button('Открыть список игр', color=VkKeyboardColor.POSITIVE, payload={"menu": "main", "action": "list-games", "msg_id": msg_id + 1})
    keyboard.add_line()
    keyboard.add_button('Создать игру', color=VkKeyboardColor.POSITIVE, payload={"menu": "main", "action": "add-game", "msg_id": msg_id + 1})
    keyboard.add_line()
    keyboard.add_button('Выйти', color=VkKeyboardColor.NEGATIVE, payload={"menu": "main", "action": "exit", "msg_id": msg_id + 1})

    return keyboard.get_keyboard()
