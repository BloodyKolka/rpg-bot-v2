from vk_api.utils import get_random_id

from action_handlers.add_game import handle_add_game_action
from game_data.player_info import PlayerInfo
from menus.list_games.header import get_list_games_header
from menus.list_games.vk.content import create_vk_list_games_menu
from vk.utils import vk_delete_message
from vk.vars import vk
from vk.vars import vk_lock


def handle_vk_main_menu(payload, player: PlayerInfo, msg_id):
    vk_delete_message(player.id, payload["msg_id"])

    if payload["action"] == 'list-games':
        header = get_list_games_header()
        menu = create_vk_list_games_menu(msg_id)
        with vk_lock:
            vk.messages.send(user_id=player.id, message=header, keyboard=menu, random_id=get_random_id())
    elif payload["action"] == 'add-game':
        path = f'./data/sessions/vk/{player.id}'
        with vk_lock:
            vk.messages.send(user_id=player.id, message=handle_add_game_action('', path, player), random_id=get_random_id())
    elif payload["action"] == 'exit':
        with vk_lock:
            vk.messages.send(user_id=player.id, message='Пока 👋! Если ещё понадоблюсь, введи /start',
                             random_id=get_random_id())
