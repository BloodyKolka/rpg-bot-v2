import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.session_utils import save_session


def handle_edit_game_action(text: str, session_dir: str, player: PlayerInfo):
    session = get_session(session_dir, "edit-game")

    game_id = int(session["game_id"])
    result = gdg.game_manager.get_game(game_id)
    if not result.ok:
        return result.desc
    game = result.res

    msg = 'ERROR'
    if session["stage"] == 'compose':
        if session["parameter"] == 'slots':
            try:
                value = int(text)
                if value < (game.total_slots - game.free_slots):
                    return 'Упс! На игру уже записалось больше игроков, чем новый лимит. Если всё равно хотите уменьшить число игроков - сначала попроси их отменить запись или свяжись с администратором. Чтобы отменить изменение введи команду /cancel'
            except:
                return 'Упс! Кажется, Вы отправили что-то кроме числа. Количество мест на игре должно быть числом без дополнительных символов'
        elif session["parameter"] == 'session':
            try:
                value = int(text)
            except:
                return 'Упс! Кажется, Вы отправили что-то кроме числа. Номер сессии должен быть числом без дополнительных символов'
        else:
            value = text
        session["value"] = value
        session["stage"] = 'confirm'

        msg = 'Отлично! Проверь ещё раз и напишите <code>Подтверждаю</code>, чтобы применить изменения. Если отправишь что-то другое - изменения будут отменены'
        save_session(session_dir, session)
    elif session["stage"] == "confirm":
        if text.lower() != 'подтверждаю':
            msg = 'Вы отправили сообщение, отличное от "Подтверждаю" - редактирование игры отменено'
        else:
            if session["parameter"] == 'system_short':
                game.system_info.short_name = session["value"]
            elif session["parameter"] == 'system_full':
                game.system_info.full_name = session["value"]
            elif session["parameter"] == 'campaign_name':
                game.campaign_info.name = session["value"]
            elif session["parameter"] == 'campaign_desc':
                game.campaign_info.desc = session["value"]
            elif session["parameter"] == 'scenario_name':
                game.session_info.name = session["value"]
            elif session["parameter"] == 'scenario_desc':
                game.session_info.desc = session["value"]
            elif session["parameter"] == 'session':
                game.campaign_info.session = session["value"]
            elif session["parameter"] == 'slots':
                game.free_slots = session["value"] - (game.total_slots - game.free_slots)
                game.total_slots = session["value"]
            elif session["parameter"] == 'date_time':
                game.date_time = session["value"]
            elif session["parameter"] == 'place':
                game.place = session["value"]
            elif session["parameter"] == 'gm_name':
                game.gm_info.name = session["value"]
            result = gdg.game_manager.update_game(game)
            if not result.ok:
                msg = result.desc
            else:
                msg = 'Игра успешно изменена!'

        delete_session(session_dir)

    return msg
