import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo

import poll_data.global_vars as pdg
from poll_data.game_poll import GamePoll

from utils.send_message import send_message
from utils.send_poll import send_poll

from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.session_utils import save_session

from utils.html_special import replace_html_special


def handle_message_players_action(text: str, session_dir: str, _unused_: PlayerInfo):
    session = get_session(session_dir, "message-players")

    msg = 'ERROR'
    if session["stage"] == "poll":
        msg = 'Отлично! Теперь напишите сообщение, которое будет перед опросом'

        session["stage"] = "compose"
        save_session(session_dir, session)
    elif session["stage"] == "compose":
        session["message"] = text
        msg = 'Хорошо сказано! Проверьте ещё раз и отправьте сообщение <code>Подтверждаю</code>, чтобы отправить сообщение игрокам. Если Вы отправите что-то другое - то отправка будет отменена.'

        session["stage"] = "confirm"
        save_session(session_dir, session)
    elif session["stage"] == "confirm":
        if text.lower() != 'подтверждаю':
            return 'Отправка сообщение отменена!'

        game_id = int(session["game_id"])
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            return result.desc
        game = result.res

        poll = None
        if 'poll' in session:
            poll = GamePoll.from_json(session["poll"])
            poll = pdg.poll_manager.add_poll(poll)
            msg = 'Сообщение и опрос отправлены!'
        else:
            msg = 'Сообщение отправлено!'

        message = replace_html_special(session["message"])
        for player in game.players:
            if poll is not None:
                send_message(f"Привет! Ведущий отправил опрос по игре, на которую Вы записаны.\n{replace_html_special(game.get_header())}", player)
                send_message(message, player)
                send_poll(poll, player)
            else:
                send_message(f"Привет! Ведущий просил(а) передать информацию об игре, на которую Вы записаны.\n{replace_html_special(game.get_header())}", player)
                send_message(message, player)

        delete_session(session_dir)

    return msg
