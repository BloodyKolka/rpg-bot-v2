import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from utils.session_utils import delete_session
from utils.session_utils import get_session


def handle_delete_game_action(text: str, session_dir: str, player: PlayerInfo):
    session = get_session(session_dir, "delete-game")

    if text.lower() == "подтверждаю":
        result = gdg.game_manager.delete_game(int(session["game_id"]))
        msg = result.desc
    else:
        msg = 'Вы отправили сообщение, отличное от "Подтверждаю" - удаление игры отменено'

    delete_session(session_dir)
    return msg
