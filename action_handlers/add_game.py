import game_data.global_vars as gdg
from game_data.game import Game
from game_data.player_info import PlayerInfo
from utils.session_utils import delete_session, get_session, save_session


def handle_add_game_action(text: str, session_dir: str, player: PlayerInfo) -> str:
    session = get_session(session_dir, "add-game")

    msg = "ERROR"
    if session["stage"] == "initial":
        msg = 'Напишите, пожалуйста, краткое название системы (например, <code>D&amp;D 5e</code>)'
        session["stage"] = 'system_short'
    elif session["stage"] == 'system_short':
        session["game_info"] = dict()
        session["game_info"]["game_id"] = 0
        session["game_info"]["gm_info"] = player.to_json()
        session["game_info"]["system_info"] = dict()
        session["game_info"]["system_info"]["short_name"] = text

        msg = 'Отлично, теперь напишите полное название системы (например <code>Dungeons &amp; Dragons 5-я редакция</code>)'
        session["stage"] = 'system_full'
    elif session["stage"] == 'system_full':
        session["game_info"]["system_info"]["full_name"] = text
        msg = 'Отлично, теперь напишите название кампании, если это - кампания, или слово <code>ваншот</code>, если это - ваншот'

        session["stage"] = 'campaign_name'
    elif session["stage"] == 'campaign_name':
        session["game_info"]["campaign_info"] = dict()
        if text.lower() == 'ваншот':
            session["game_info"]["campaign_info"]["is_oneshot"] = True
            session["game_info"]["campaign_info"]["name"] = ''
            session["game_info"]["campaign_info"]["desc"] = ''
            session["game_info"]["campaign_info"]["session"] = 0

            msg = "Отлично, теперь напишите название сессии"
            session["stage"] = 'session_name'
        else:
            session["game_info"]["campaign_info"]["is_oneshot"] = False
            session["game_info"]["campaign_info"]["name"] = text
            session["game_info"]["campaign_info"]["session"] = 1

            msg = "Отлично, теперь напишите описание кампании"
            session["stage"] = 'campaign_desc'
    elif session["stage"] == 'campaign_desc':
        session["game_info"]["campaign_info"]["desc"] = text
        msg = "Отлично, теперь напишите название сессии"
        session["stage"] = 'session_name'
    elif session["stage"] == 'session_name':
        session["game_info"]["session_info"] = dict()
        session["game_info"]["session_info"]["name"] = text

        msg = 'Отлично, теперь напишите описание сессии'
        session["stage"] = 'session_desc'
    elif session["stage"] == 'session_desc':
        session["game_info"]["session_info"]["desc"] = text

        msg = 'Отлично, теперь напишите максимальное кол-во игроков'
        session["stage"] = 'slots'
    elif session["stage"] == 'slots':
        try:
            slots = int(text)
        except:
            return 'Упс! Кажется, Вы написали что-то кроме числа. Количество мест на игре должно быть числом без дополнительных символов'

        if slots <= 0:
            return 'Упс! Количество мест должно быть положительным числом'

        session["game_info"]["total_slots"] = slots
        session["game_info"]["free_slots"] = slots
        session["game_info"]["players"] = []
        session["game_info"]["registration_open"] = True

        msg = 'Отлично, теперь напишите, когда состоится игра (например, "завтра", "03.10.2022 11:00" или "пока не знаю")'
        session["stage"] = 'date_time'
    elif session["stage"] == 'date_time':
        session["game_info"]["date_time"] = text

        msg = 'Отлично, теперь напишите место встречи (например, "в клубе Х", "у меня дома" или "пока не знаю")'
        session["stage"] = 'place'
    elif session["stage"] == 'place':
        session["game_info"]["place"] = text

        msg = f'Отлично, теперь напишите как мне подписать Вас в поле ведущий. Можете использовать своё имя профиля: <code>{player.name}</code>'
        session["stage"] = 'gm_name'
    elif session["stage"] == 'gm_name':
        session["game_info"]["gm_info"]["name"] = text
        session["stage"] = 'finished'

        msg = 'Отлично, я всё записал!'

    if session["stage"] == 'finished':
        game = Game.from_json(session["game_info"])
        gdg.game_manager.add_game(game)
        delete_session(session_dir)
    else:
        save_session(session_dir, session)

    return msg
