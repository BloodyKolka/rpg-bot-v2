import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo

from utils.send_message import send_message
from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.html_special import replace_html_special

def handle_kick_player_action(text: str, session_dir: str, player: PlayerInfo):
    session = get_session(session_dir, "kick-player")

    if text.lower() == "подтверждаю":
        game_id = int(session["game_id"])
        player_num = int(session["player_num"])
        result = gdg.game_manager.get_game(game_id)
        if not result.ok:
            delete_session(session_dir)
            return result.desc
        game = result.res

        player = game.players[player_num]
        game.kick_player(player_num)
        gdg.game_manager.update_game(game)

        send_message(f'Вас выгнали с игры\n{replace_html_special(game.get_header())}', player)

        msg = 'Игрок успешно выгнан!'
    else:
        msg = 'Вы отправили сообщение, отличное от "Подтверждаю" - изнание ирока отменено'

    delete_session(session_dir)

    return msg
