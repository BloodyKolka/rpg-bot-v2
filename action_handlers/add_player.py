import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from game_data.player_info import is_same_user
from utils.get_player_tag import get_player_tag
from utils.send_message import send_message
from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.session_utils import save_session
from utils.html_special import replace_html_special

def handle_add_player_action(text: str, session_dir: str, player: PlayerInfo):
    session = get_session(session_dir, "add-player")

    msg = 'ERROR'
    if session["stage"] == 'compose':
        session["name"] = text
        session["stage"] = 'confirm'
        save_session(session_dir, session)

        msg = 'Отлично! Проверьте ещё раз и напишите <code>Подтверждаю</code>, чтобы подтвердить. Если отправите что-то другое - добавление будет отменено'
    elif session["stage"] == 'confirm':
        if text.lower() != 'подтверждаю':
            msg = 'Вы отправили сообщение, отличное от "Подтверждаю" - редактирование игры отменено'
        else:
            game_id = int(session["game_id"])
            result = gdg.game_manager.get_game(game_id)
            if not result.ok:
                delete_session(session_dir)
                return result.desc
            game = result.res

            result = game.add_player(session["name"], player)
            if not result.ok:
                delete_session(session_dir)
                return result.desc

            result = gdg.game_manager.update_game(game)
            if not result.ok:
                delete_session(session_dir)
                return result.desc

            if not is_same_user(game.gm_info, player):
                gm_msg = f"Привет, обновление по Вашей игре\n{replace_html_special(game.get_header())}\n"
                gm_msg += f'Игрок {replace_html_special(player.name)} ({get_player_tag(player, game.gm_info.platform)}) зарегистрировал на Вашу игру +1 человека - {replace_html_special(session["name"])}'
                send_message(gm_msg, game.gm_info)

            msg = 'Игрок добавлен!'
        delete_session(session_dir)

    return msg
