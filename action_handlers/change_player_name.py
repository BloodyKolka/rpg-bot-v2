import game_data.global_vars as gdg
from game_data.player_info import PlayerInfo
from utils.session_utils import delete_session
from utils.session_utils import get_session
from utils.session_utils import save_session


def handle_change_player_name_action(text: str, session_dir: str, player: PlayerInfo):
    session = get_session(session_dir, "change-player-name")

    msg = 'ERROR'
    if session["stage"] == 'compose':
        session['new_name'] = text
        session["stage"] = 'confirm'
        save_session(session_dir, session)

        msg = 'Отлично! Проверьте ещё раз и напишите <code>Подтверждаю</code>, чтобы применить изменения. Если отправите что-то другое - изменения будут отменены'
    elif session["stage"] == 'confirm':
        if text.lower() != 'подтверждаю':
            msg = 'Вы отправили сообщение, отличное от "Подтверждаю" - редактирование игры отменено'
        else:
            game_id = int(session["game_id"])
            result = gdg.game_manager.get_game(game_id)
            if not result.ok:
                delete_session(session_dir)
                return result.desc
            game = result.res

            player.name = session['new_name']
            result = game.update_player_name(player)
            if not result.ok:
                delete_session(session_dir)
                return result.desc

            result = gdg.game_manager.update_game(game)
            if not result.ok:
                delete_session(session_dir)
                return result.desc

            msg = 'Ваше имя обновлено!'
        delete_session(session_dir)

    return msg
